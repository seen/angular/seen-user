/*
 * Copyright (c) 2015 Phoenix Scholars Co. (http://dpq.co.ir)
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('TestModule', [ 'seen-test', 'seen-user' ]);

describe('Factories ', function() {
	var $usr;
	var $test;

	beforeEach(function() {
		module('TestModule');
		inject(function(_$usr_, _$test_) {
			$usr = _$usr_;
			$test = _$test_;
		});
	});

	function testForResource(factory, resource) {
		it('should contains function of ' + resource.name, function() {
			$test.factoryCollectionFunctions(factory, resource);
		});
	}

	function testFor(factory) {
		// Function test
		it('should contains basic functions', function() {
			$test.factoryBasicFunctions(factory);
		});

		it('should call POST:' + factory.url + ' to update', function(done) {
			$test.factoryUpdateFunctions(factory, done);
		});
		it('should call DELETE:' + factory.url + ' to update', function(done) {
			$test.factoryDeleteFunctions(factory, done);
		});

		if (angular.isArray(factory.resources)) {
			for (var j = 0; j < factory.resources.length; j++) {
				testForResource(factory, factory.resources[j]);
			}
		}
	}

	var factories = [ {
		factory : 'UserAccount',
		url : '/api/v2/user/accounts',
		resources : [ {
			name : 'Group',
			factory : 'UserGroup',
			url : '/groups'
		}, {
			name : 'Role',
			factory : 'UserRole',
			url : '/roles'
		} ]
	}, {
		factory : 'UserRole',
		url : '/api/v2/user/roles',
		resources : [ {
			name : 'Group',
			factory : 'UserGroup',
			url : '/groups'
		}, {
			name : 'Account',
			factory : 'UserAccount',
			url : '/accounts'
		} ]
	}, {
		factory : 'UserGroup',
		url : '/api/v2/user/groups',
		resources : [ {
			name : 'Account',
			factory : 'UserAccounts',
			url : '/accounts'
		}, {
			name : 'Role',
			factory : 'UserRole',
			url : '/roles'
		} ]
	}, {
		factory: 'UserOAuth2Server',
		url: '/api/v2/user/oauth2-servers'
	}, {
		factory: 'UserOAuth2Connection', 
		url: '/api/v2/user/oauth2-connections'
	}];
	for (var i = 0; i < factories.length; i++) {
		testFor(factories[i]);
	}
});
