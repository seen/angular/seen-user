# Seen User

master:
[![pipeline status](https://gitlab.com/seen/angular/seen-user/badges/master/pipeline.svg)](https://gitlab.com/seen/angular/seen-user/commits/master) 
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/c1369cf4f76244fea66cd5ede6099552)](https://www.codacy.com/app/seen/seen-user?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=seen/angular/seen-user&amp;utm_campaign=Badge_Grade)

develop:
[![pipeline status - develop](https://gitlab.com/seen/angular/seen-user/badges/develop/pipeline.svg)](https://gitlab.com/seen/angular/seen-user/commits/develop)